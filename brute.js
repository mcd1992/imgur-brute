#!/bin/env node

require("./stringmethods"); // Import extra String class methods

// Returns an array of all possible case/capitalization permutations of a given word
function GetCasePermutations(word) {
    if (!word || (typeof(word) != 'string')) {
        return [];
    }
    word = word.toLowerCase(); // Start with the lowercase form

    var permutations = [];
    permutations.push(word); // Add the lowercase form

    // Since the alphabet's case is binary we can use a bitmask to 'bruteforce' the possible case permutations
    var wordlen = word.length;
    var maxint  = Math.pow(2, wordlen); // The integer required to reach our desired bitmap 'width'
    for (var bitmask = 1; bitmask < maxint; bitmask++) {
        permutations.push(word.applyCaseMask(bitmask));
    }

    return permutations;
}

console.log(GetCasePermutations("THIS TEST"));
