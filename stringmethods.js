// Replaces a character at the given index with newchar
String.prototype.replaceCharAt = function(index, newchar) {
    if (!newchar || (newchar.length > 1)) {
        console.trace("String.replaceCharAt called with invalid args");
        return this;
    }

    return this.substr(0, index) + newchar + this.substr(index+1);
}

// A new method for applying a case sensitivity bitmask to a string
// The bitmask will be an integer. A 1 will represent uppercase, 0 lowercase.
String.prototype.applyCaseMask = function(bitmask) {
    var retstring = this.toLowerCase();
    if (!bitmask) {
        console.trace("String.applyCaseMask called without bitmask");
        return retstring;
    }

    var stringlen = retstring.length;
    for (var index = stringlen - 1; index > -1; index--) { // Compare every character with the given case bitmask
        if (bitmask & 1) { // This character needs to be uppercase
            retstring = retstring.replaceCharAt(index, retstring.charAt(index).toUpperCase());
        }
        bitmask = bitmask >> 1; // Shift the bitmask to the right
    }

    return retstring;
};
